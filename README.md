#### `npm start` or `yarn start` to start server

#### `npm run test` or `yarn test` after that in another console to start test (I'll write a script for more comfortable use later)

http://localhost:5000/stock/msft

`curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:5000/stock/msft`
