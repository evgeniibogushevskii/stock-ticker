const http = require('http');
const router = require('./router');
const appLog = require('./utils/appLog');

const port = 5000;

const requestHandler = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  router(req)
    .then(data => {
      res.write(JSON.stringify(data));
      appLog(req, res);
    })
    .catch(err => {
      res.write(`${err}`);
      appLog(req, res, err.message);
    })
    .finally(() => {
      res.end();
    });
};

const server = http.createServer(requestHandler);

server.listen(port, err => {
  if (err) {
    console.log(`Oops... ${err}`);
  }

  console.log(`Server is listening on port ${port}`);
});
