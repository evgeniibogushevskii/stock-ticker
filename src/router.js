const routeHandlers = require('./routes/routeHandlers');
const errorHandler = require('./utils/errorHandler');

const router = async req => {
  const neededRoute = routeHandlers.filter(routeHandler => {
    return routeHandler.reg.test(req.url);
  });

  if (neededRoute.length === 0) {
    throw errorHandler('No route handlers');
  }

  return neededRoute[0].handler(req.url);
};

module.exports = router;
