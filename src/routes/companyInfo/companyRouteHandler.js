const getLatestPrice = require('./latestPrice');
const getLogoLink = require('./logoLink');
const getLatestNewsLink = require('./latestNews');

const companyRouteHandler = {
  route: 'stock/{tickerSymbol}',
  reg: /(\/stock\/)([^\s]+)/i,
  handler: async url => {
    const tickerSymbol = url.match(/(?:(?![\/])[\w]+$)/)[0];
    const [latestPrice, logoLink, latestNewsLink] = await Promise.all([
      getLatestPrice(tickerSymbol),
      getLogoLink(tickerSymbol),
      getLatestNewsLink(tickerSymbol)
    ]);
    return { latestPrice, logoLink, latestNewsLink };
  }
};

module.exports = companyRouteHandler;
