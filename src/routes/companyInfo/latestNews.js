const get = require('../../utils/get');

const getLatestNewsLink = async tickerSymbol => {
  const companyData = await get(`/stock/${tickerSymbol}/news/last/1`);
  return companyData[0].url;
};

module.exports = getLatestNewsLink;
