const get = require('../../utils/get');

const getLatestPrice = async tickerSymbol => {
  const companyData = await get(`/stock/${tickerSymbol}/quote`);
  return companyData.latestPrice;
};

module.exports = getLatestPrice;
