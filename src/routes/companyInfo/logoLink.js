const get = require('../../utils/get');

const getLogoLink = async tickerSymbol => {
  const companyData = await get(`/stock/${tickerSymbol}/logo`);
  return companyData.url;
};

module.exports = getLogoLink;
