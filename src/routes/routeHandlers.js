const companyRouteHandler = require('./companyInfo/companyRouteHandler');

const routeHandlers = [companyRouteHandler];

module.exports = routeHandlers;
