const companyRouteHandler = require('../routes/companyInfo/companyRouteHandler');

const testCompanyRoute = () => {
  if (companyRouteHandler.reg.test('/stock/msft')) {
    console.log('Test passed!');
    return;
  }
  console.log('Test failed!');
};

module.exports = testCompanyRoute;
