const errorHandler = require('../utils/errorHandler');

const testError = () => {
  const error = errorHandler('Some error message');
  if (error instanceof Error) {
    console.log('Test passed!');
    return;
  }
  console.log('Test failed!');
};

module.exports = testError;
