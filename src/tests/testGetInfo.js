const getLatestPrice = require('../routes/companyInfo/latestPrice');
const getLogoLink = require('../routes/companyInfo/logoLink');
const getLatestNewsLink = require('../routes/companyInfo/latestNews');

const testGetInfo = async tickerSymbol => {
  const [latestPrice, logoLink, latestNewsLink] = await Promise.all([
    getLatestPrice(tickerSymbol),
    getLogoLink(tickerSymbol),
    getLatestNewsLink(tickerSymbol)
  ]);

  if (
    typeof latestPrice === 'number' &&
    typeof logoLink === 'string' &&
    typeof latestNewsLink === 'string'
  ) {
    console.log('Test passed!');
    return;
  }
  console.error('Test failed!');
};

module.exports = testGetInfo;
