const testCompanyRoute = require('./testCompanyRoute');
const testGetInfo = require('./testGetInfo');
const testError = require('./testError');

const tests = async () => {
  testCompanyRoute();
  await testGetInfo('msft');
  testError();
};

tests();
