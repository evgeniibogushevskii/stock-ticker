const fs = require('fs');

const toTwoDigits = number => ('0' + number).slice(-2);

const getDateTime = () => {
  const now = new Date();
  const date = `${toTwoDigits(now.getMonth() + 1)}/${toTwoDigits(
    now.getDate()
  )}/${toTwoDigits(now.getFullYear())}`;
  const time = `${toTwoDigits(now.getHours())}:${toTwoDigits(
    now.getMinutes()
  )}:${toTwoDigits(now.getSeconds())}`;
  return `${date} ${time}`;
};

const logData = data => {
  fs.appendFileSync('app.log', `${data}\n`);
};

const appLog = (req, res, error) => {
  const dateTime = getDateTime();
  let data = `  Request URL: ${req.url}
  Date and time of the request: ${dateTime}
  Response status code: ${res.statusCode} 
  ${error ? 'Error: ' + error : 'Request fulfilled successfully'}\n`;
  logData(data);
};

module.exports = appLog;
