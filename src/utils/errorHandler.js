const errorHandler = errorInfo => {
  const error = new Error(errorInfo);
  return error;
};

module.exports = errorHandler;
