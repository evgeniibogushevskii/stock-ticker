const https = require('https');
const errorHandler = require('./errorHandler');

const baseURL = 'https://api.iextrading.com/1.0';

const get = url => {
  return new Promise((resolve, reject) => {
    https
      .get(`${baseURL}${url}`, res => {
        let body = '';
        res.on('data', chunk => {
          body += chunk;
        });

        res.on('end', () => {
          try {
            const data = JSON.parse(body);
            resolve(data);
          } catch (err) {
            reject(errorHandler('Unable to parse response data'));
          }
        });
      })
      .on('error', err => {
        reject(errorHandler(`${err}`));
      });
  });
};

module.exports = get;
